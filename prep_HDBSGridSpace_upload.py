import os
import re

import subprocess   # for executing bash commands
import argparse # to parse command line options
import warnings # to warn about things
import hashlib # to perform file hashing
import pandas as pd
from io import StringIO
import csv


def writeGridLocationToFile(datasetName , outputFileName = "datasetNameOnGrid.txt"): # let's leave a .txt file behind, that tells us the dataset name for our files on the grid
    # use outputFileName to store standard file name for later retrieval and global storage of tha variable

    if datasetName is not None:
        with open("datasetNameOnGrid.txt", "w") as outputFile:     
            outputFile.write("# files here have been uploaded to the grid \n")
            outputFile.write("# they are available with the dataset name \n")
            outputFile.write(datasetName+"\n\n")

            outputFile.write("Create symlinks on LXPlus for the files in this dataset via\n\n")
            outputFile.write("rucio list-file-replicas --rse CERN-PROD_PHYS-HDBS --link /eos/:/eos/ "+datasetName +"\n")
            #outputFile.writelines(L)

    return outputFileName



    #dataset = "user.chweber:user.chweber.ZdZd.production_20190527_da771f51.ZdZd13TeV.bkg_mc16e_ttH_NTUP4L"
    ##rucioCommand = ["rucio", "list-files", "--rse", "CERN-PROD_SCRATCHDISK", dataset]
    #rucioCommand = ["rucio", "list-files", dataset]
    #rucioOutput = subprocess.check_output( rucioCommand )



def parseRucioOutput(rucioListFilesReturn, dataset = None):

    matchedFileNames=[]

    if dataset is None:  scope = "((user)|(group))\.\S+?:"
    else:                scope = re.match("\S+?\:",dataset).group()

    for line in  rucioListFilesReturn.splitlines(): # let's parse the output line by line
        fileMatch = re.search( scope +"\S+" , line) # files are like <scope> and some non-whitespace characters
        if fileMatch: 
            fileNameWithoutScope = re.sub( scope , "" , fileMatch.group()) # need to remove the scope from the extracted file name
            matchedFileNames.append( fileNameWithoutScope )

    return matchedFileNames

def rucioListFilesInDataset(dataset):

    rucioCommand = ["rucio", "list-files", dataset]
    rucioOutput = subprocess.check_output( rucioCommand )

    return parseRucioOutput(rucioOutput, dataset = dataset)

def rucioIsDatasetOnGrid(dataset):
    rucioReturn = subprocess.check_output( ["rucio", "list-dids", dataset] )
    isOnGrid = dataset in rucioReturn
    return isOnGrid

def makeTargetRucioDataset(folderWithFiles, rucioTargetPrefix):

    currentFolder = folderWithFiles.split("/")[-1]
    # the word 'user' or the word 'group' followed by a dot '.' any number of any character, until we find another dot '.' (including the last dot)
    reSearchString = "((user)|(group))\..+?\."
    #re.match(reSearchString ,currentFolder).group() # use this to thest the search string
    targetDataset = re.sub(reSearchString, rucioTargetPrefix ,currentFolder)
    print(targetDataset)

    return targetDataset

def uploadFilesToGrid(folderWithFiles, rucioTargetPrefix, filesOnDisk):

    #import pdb; pdb.set_trace()   ## enter here with the debugger

    if len(filesOnDisk) == 0 : return None

    previousWorkingDir = os.getcwd() # presume this is the folder where we downloaded all the samples from the grid

    os.chdir(folderWithFiles) 


    targetDataset = makeTargetRucioDataset(folderWithFiles, rucioTargetPrefix)


    if rucioIsDatasetOnGrid(targetDataset): filesOnGrid = rucioListFilesInDataset(targetDataset)
    else:                                   filesOnGrid = []

    filesToUpload = list(set(filesOnDisk) - set(filesOnGrid))


    for file in filesToUpload:
        if os.path.islink(file): continue # don't actually upload files that are represented by symbolic links for now

        #import pdb; pdb.set_trace()   ## enter here with the debugger
        rucioUploadCommand = ["rucio", "upload", "--rse", "CERN-PROD_SCRATCHDISK", targetDataset, file]
        os.system( " ".join(rucioUploadCommand) )


    # RE string for datasets "((user)|(group))\.\S+:\S+"

    #rucioUploadCommand = ["rucio", "upload", "--rse", "CERN-PROD_SCRATCHDISK", targetDataset]
    #rucioUploadCommand.extend(filesToUpload)
    #
    #
    #os.system( " ".join(rucioUploadCommand) )
    ## use subprocess.check_output instead, if I want to know the statys of the operation https://stackabuse.com/executing-shell-commands-with-python/
    ## rucioResult = subprocess.check_output( rucioUploadCommand )
    
    writeGridLocationToFile(targetDataset)

    #import pdb; pdb.set_trace()   ## enter here with the debugger


    os.chdir(previousWorkingDir) 

    return None


def prinFileToScreen(textFile):
    file = open(textFile, 'r')
    file_contents = file.read()
    print(file_contents)
    file.close()
    return None


def checkForRSE(datasetsToCheck, targetRSE = "CERN-PROD_PHYS-HDBS"): # RSE = rucio storage element

    #import pdb; pdb.set_trace()   ## enter here with the debugger

    replicatedToRSE = []
    notReplicatedDatasets = []

    notOnGrid = []



    for dataset in datasetsToCheck:

        if not rucioIsDatasetOnGrid(dataset):
            notOnGrid.append(dataset)
            continue


        rucioReturn = subprocess.check_output(["rucio", "list-dataset-replicas", dataset])

        if targetRSE in rucioReturn: replicatedToRSE.append(dataset +"\n")
        else:                        notReplicatedDatasets.append(dataset +"\n")

    # write out the results to a file
    ouputFileName = "RSE_check_results_%s.txt" %targetRSE
    with open(ouputFileName, "w") as outputFile:     
        outputFile.write("Datasets on " + targetRSE + " : \n")
        outputFile.writelines(replicatedToRSE)

        outputFile.write("\nDataset left unassined to " + targetRSE + " :\n")
        outputFile.write("I should setup a replication rule for them on 'https://rucio-ui.cern.ch/r2d2/request' : \n")
        outputFile.writelines(notReplicatedDatasets)
        #outputFile.writelines(L)

    prinFileToScreen(ouputFileName)

    #import pdb; pdb.set_trace()   ## enter here with the debugger

    overviewDict = {targetRSE : replicatedToRSE, "not in %s" %targetRSE : notReplicatedDatasets}
    return overviewDict

def onlyROOTFiles( fileList): return [file for file in fileList if file.endswith(".root")]


def assertInferredFileNameIsCorrect(folderWithFiles, targetDataset):
    # we are storing the datasetname also in a file, let's make sure the the inferred name and the stored one are identical

    fileStoringDatasetName = writeGridLocationToFile( None) 
    fileStoringDatasetNameAbsPath = os.path.join( folderWithFiles, fileStoringDatasetName  )

    assert os.path.exists( fileStoringDatasetNameAbsPath  ) 

    file = open( fileStoringDatasetNameAbsPath , 'r')
    file_contents = file.read()
    file.close()

    assert targetDataset in file_contents

    return True


def rucioCheckReplicationRules( targetDataset ):


    rucioReturn = subprocess.check_output(["rucio", "list-rules", targetDataset])

    assert "phys-hdbs" in rucioReturn

    lineOfInterest = re.search( ".+phys-hdbs.+" ,rucioReturn).group() # get the line that contains "phys-hdbs"

    # let's make sure all files in the dataset have been correctly transferred to the phys-hdbs space
    # we are looking for the STATE column which: STATE[OK/REPL/STUCK]
    # which should be like OK[X/0/0], wheher X is any integer number, so our regex tag is 

    regexTag = "OK\[\d+\/0\/0\]"

    allTransferredToPhys_HDBS = re.search(regexTag, lineOfInterest)

    assert allTransferredToPhys_HDBS

    return None


def getEOSLocationOfSampleOnCERN_PROD_PHYS_HDBS(targetDataset,fileName):

    scope = re.match("\S+:",targetDataset).group()

    rucioReturn = subprocess.check_output(["rucio", "list-file-replicas", scope+fileName])

    lineOfInterest = re.search( ".+CERN-PROD_PHYS-HDBS.+", rucioReturn).group() # get the line that pertains to the  CERN-PROD_PHYS-HDBS RSE

    EOSPath = re.search("\/eos\/\S+"+fileName,lineOfInterest).group()

    return EOSPath

def getMD5Hash(fileLocation): # https://www.pythoncentral.io/hashing-files-with-python/

    BLOCKSIZE = 65536
    hasher = hashlib.md5()
    with open(fileLocation, 'rb') as afile:
        buf = afile.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(BLOCKSIZE)

    return hasher.hexdigest()



def checkAllFilesProperlyOnGridAndReplaceWithSymlinks(folderWithFiles, rucioTargetPrefix, filesOnDisk):

    print(folderWithFiles)

    previousWorkingDir = os.getcwd() # presume this is the folder where we downloaded all the samples from the grid
    os.chdir(folderWithFiles) 


    targetDataset = makeTargetRucioDataset(folderWithFiles, rucioTargetPrefix)

    assertInferredFileNameIsCorrect(folderWithFiles, targetDataset)

    rucioCheckReplicationRules( targetDataset )

    filesOnGrid = rucioListFilesInDataset(targetDataset)
    filesToUpload = list(set(filesOnDisk) - set(filesOnGrid))

    assert len(set(filesOnDisk) - set(filesOnGrid)) == 0 # make sure all the files we have here are actually on the grid

    for file in filesOnDisk:

        if os.path.islink(file): continue # no need to check that root files represented by sumlinks, are properly on the grid. We assume they are.

        fileEOSLocation = getEOSLocationOfSampleOnCERN_PROD_PHYS_HDBS(targetDataset, file)


        assert getMD5Hash( file ) == getMD5Hash( fileEOSLocation )

        print("MD5 match with grid for: \t" +file + "\t, replacing with symlink")

        os.remove(file)

        os.symlink(fileEOSLocation, file)

    os.chdir(previousWorkingDir) 


    return None


def inferDatasetPrefix(filePath):

    if "mini_ntuple" in filePath: return False
    elif "4tau" in filePath or "4Tau" in filePath: return "user.chweber.4Tau.production_"

    else: return "user.chweber.ZdZd.production_"


def check_RSE_for_TAPE_and_CERN_PROD_PHYS_HDBS(datasetsToCheck):

    outputFileDict = {}

    for RSE_to_check in ["CERN-PROD_PHYS-HDBS","BNL-OSG2_GROUPTAPE" ]:
        outputFileDict[RSE_to_check] = checkForRSE(datasetsToCheck, targetRSE = RSE_to_check)

    #import pdb; pdb.set_trace()   ## enter here with the debugger

    filesToTransferToTape = list( 
        set(outputFileDict["CERN-PROD_PHYS-HDBS"]["CERN-PROD_PHYS-HDBS"]).intersection(
            outputFileDict["BNL-OSG2_GROUPTAPE"]["not in BNL-OSG2_GROUPTAPE"])
        )


    filesIMayRemoveFromHDBSGrid =list(
        set(outputFileDict["CERN-PROD_PHYS-HDBS"]["CERN-PROD_PHYS-HDBS"]).intersection(
            outputFileDict["BNL-OSG2_GROUPTAPE"]["BNL-OSG2_GROUPTAPE"])
        )

    # write out the results to a file
    ouputFileName = "TapeTransferCandidates.txt" 
    with open(ouputFileName, "w") as outputFile:     
        outputFile.write("Datasets I may transfer to tape, \n")
        outputFile.write("i.e. they are on CERN-PROD_PHYS-HDBS, but not on BNL-OSG2_GROUPTAPE :\n")
        outputFile.writelines(filesToTransferToTape)
        outputFile.write("I should setup a replication rule for them on 'https://rucio-ui.cern.ch/r2d2/request' : \n")


        outputFile.write("\nDatasets I may remove from CERN-PROD_PHYS, \n")
        outputFile.write("i.e. datasets are on both, CERN-PROD_PHYS-HDBS and BNL-OSG2_GROUPTAPE  :\n")
        outputFile.writelines(filesIMayRemoveFromHDBSGrid)

    prinFileToScreen(ouputFileName)

    return None



def make_rucioRules_spreadsheet(datasetsToCheck):

    listOfDictsForCSV = []


    for rootPath, dataset in datasetsToCheck:

        if not rucioIsDatasetOnGrid(dataset):continue

        rucioReturn = subprocess.check_output(["rucio", "list-rules", dataset])

        csvStringIO = StringIO(unicode(rucioReturn, "utf-8"))
        pandasDataFrame = pd.read_csv(csvStringIO, delim_whitespace=True)

        DSIDDict = {}
        DSIDDict["DSID"] = dataset
        DSIDDict["EOS path"] = rootPath


        for RSE, state in zip(pandasDataFrame['RSE_EXPRESSION'], pandasDataFrame["STATE[OK/REPL/STUCK]"]):

            if RSE == len(RSE) * RSE[0]: continue # some lines in the rucio return are just "-----", ignore those

            DSIDDict[RSE] = state

        listOfDictsForCSV.append(DSIDDict)

    #import pdb; pdb.set_trace()   ## enter here with the debugger

    makeCSVFile(listOfDictsForCSV, csvFileName = "RSEOverview.csv")


    return None


def makeCSVFile(csvListOfDicts, csvFileName = "csvFile.csv"):

    csvListOfDicts = sorted(csvListOfDicts, key= lambda aDict : aDict["EOS path"]  )

    ############ make sure we know about all the fields we should be writing ############
    fieldNameSet = set()
    for aDict in csvListOfDicts: 
        for key in aDict: fieldNameSet.add(key)

    fieldNameList = list(fieldNameSet)
    fieldNameList.remove("EOS path"); fieldNameList.remove("DSID")


    ############ sort the field names by hand ############

    fieldNames = ["DSID"]
    fieldNames.extend(fieldNameList)
    fieldNames.append("EOS path")

    #with open( csvFileName, 'w', encoding='UTF8', newline='') as f: # for python 3
    with open( csvFileName, 'w') as f:
        writer = csv.DictWriter(f, fieldnames=fieldNames)
        writer.writeheader()
        writer.writerows(csvListOfDicts)

    #import pdb; pdb.set_trace()   ## enter here with the debugger

    return None



# the part of the code that executes if I call this script directly
if __name__ == '__main__':
    # How to use:
    # - Upload files to scratch space via 'python prep_HDBSGridSpace_upload.py uploadFiles'
    #   change target scope to your own beforehand:
    #           rucioScope = "user.chweber:" -> rucioScope = "user.<your username>:"
    # - after successfull uploading to grid scratch space, 
    #   we need to set up a rule on rucio so that the datasets will be associated with the 
    #   'CERN-PROD_PHYS-HDBS' grid space
    #   run 'python prep_HDBSGridSpace_upload.py checkForCERN-PROD_PHYS-HDBSrse'
    #   to get a list of dataset names that are not associated yet with 'CERN-PROD_PHYS-HDBS'
    #   get a 'phys-hdbs' rucio account, and request rules for the relevant datasets to be replicated to
    #   'CERN-PROD_PHYS-HDBS' on https://rucio-ui.cern.ch/r2d2/request
    # - once all replications are done (check https://rucio-ui.cern.ch/r2d2), run 
    #   'python prep_HDBSGridSpace_upload.py ensureFilesAreOnGridAndReplaceWithSymlinks'
    #   to replace the files on EOS one by one with symlink pointing to the HDBS grid space.
    #   This will only do the replace after cross checkes passed



    workingDir = os.getcwd() # presume this is the folder where we downloaded all the samples from the grid
    #workingDir = "/afs/cern.ch/user/c/chweber/myEOS_locations/EOS_chweber/production_20200222"
    #workingDir = "/afs/cern.ch/user/c/chweber/myEOS_locations/EOS_HBSM_ZdZd/user.hcai.20200406.h2a4tau.bkg_promptOnly_mc16a_NTUPTau"

    rucioScope = "user.chweber:"

    

    #import pdb; pdb.set_trace()   ## enter here with the debugger

    # python prep_HDBSGridSpace_upload.py ensureFilesAreOnGridAndReplaceWithSymlinks


    parser = argparse.ArgumentParser()
    parser.add_argument("action", type=str, default=None , 
        choices=[ "uploadFiles",
                  "checkForCERN-PROD_PHYS-HDBSrse",
                  "ensureFilesAreOnGridAndReplaceWithSymlinks",
                  "check_RSE_for_TAPE_and_CERN-PROD_PHYS-HDBS",
                  "make_rucioRules_spreadsheet"],
        help="uploadFiles:\
        uploads files to the grid to the 'CERN-PROD_SCRATCHDISK' RSE. \
        DSID is inferred from folder name\
        Will write out the DSID into a .txt file in the folder\
        Need to manually enact new rule for it to be saved on the PHYS_HDBS rucio storage element (RSE).\
        Run the 'checkForCERN-PROD_PHYS-HDBSrse' option for help with enacting the new rules on rucio\
        \
        checkForCERN-PROD_PHYS-HDBSrse:\
        Checks all for all the files in the subdirectories, \
        whether they are replicated to the 'CERN-PROD_PHYS-HDBS' rucio storage element (RSE).\
        DSIDs are inferred from folder name\
        Outputs a .txt file 'RSE_check_results_CERN-PROD_PHYS-HDBS.txt', that lists \
        which DSIDS are on the CERN-PROD_PHYS-HDBS rucio storage element (RSE), amnd which ones are not,\
        as well with instruction on how to create the relevant replication rules\
        \
        ensureFilesAreOnGridAndReplaceWithSymlinks:\
        loops over the subdirectories, checks for each file present, \
        whether it is already replicated to the 'CERN-PROD_PHYS-HDBS' rucio storage element (RSE), \
        and if so, replaces it with a symbolic link to the relevant file on the 'CERN-PROD_PHYS-HDBS'.\
        \
        check_RSE_for_TAPE_and_CERN_PROD_PHYS_HDBS:\
        checks DSIDs are on CERN_PROD_PHYS_HDBS and on tape (BNL-OSG2_GROUPTAPE)\
        And creates a text file listing files that should be transferred to tape,\
        and that may be removed CERN-PROD_PHYS-HDBS, as they are already on tape. \
        That text file is: TapeTransferCandidates.txt\
        \
        " )

    parser.add_argument("--skipAtNthDataset", type=int, default=-1 , 
        help="If, for testing purposes, we only want N datasets, set skipAtNthDataset to N ")

    # uploadFiles - uploads files to the grid to the 'CERN-PROD_SCRATCHDISK' RSE. 
    #               DSID is inferred from folder name
    #               Will write out the DSID into a .txt file in the folder
    #               Need to manually enact new rule for it to be saved on the PHYS_HDBS RSE

    args = parser.parse_args()

    datasetsToCheck = []


    parentFoldersToSkip = ["4tau",".git"]

    nDatasets = 0


    for root, dirs, files in os.walk( workingDir ): # from the 'root' dir, walk through all the sub-'dirs' and gives a list of files in 'files'

        dirs[:] = [folder for folder in dirs if not folder in parentFoldersToSkip ]

        if len(dirs) > 0 : continue
        if len(files) == 0: continue

        #if "Truth" not in root: continue

        #import pdb; pdb.set_trace()   ## enter here with the debugger

        #if "datasetNameOnGrid.txt" in files: continue # that means we already uploaded the files
        if "skipGridUpload.txt" in files: continue # that means we already uploaded the files, or want to ignore it for some reason


        datasetPrefix = inferDatasetPrefix(root)

        if not datasetPrefix: continue


        rucioTargetPrefix = rucioScope + datasetPrefix

        if args.action == "uploadFiles" : 
            uploadFilesToGrid(root, rucioTargetPrefix, onlyROOTFiles( files) )
        elif args.action == "checkForCERN-PROD_PHYS-HDBSrse":  
            datasetsToCheck.append( makeTargetRucioDataset(root,rucioTargetPrefix) )
        elif args.action == "check_RSE_for_TAPE_and_CERN-PROD_PHYS-HDBS": 
            datasetsToCheck.append( makeTargetRucioDataset(root,rucioTargetPrefix) )
        elif args.action == "make_rucioRules_spreadsheet": 
            datasetsToCheck.append( (root, makeTargetRucioDataset(root,rucioTargetPrefix)) )
        elif args.action == "ensureFilesAreOnGridAndReplaceWithSymlinks":  
            checkAllFilesProperlyOnGridAndReplaceWithSymlinks(root, rucioTargetPrefix, onlyROOTFiles( files) )
        else: warnings.warn("Choice not defined")

        nDatasets +=1
        if args.skipAtNthDataset == nDatasets: break
        
        #import pdb; pdb.set_trace()   ## enter here with the debugger

    #import pdb; pdb.set_trace()   ## enter here with the debugger


    if args.action == "checkForCERN-PROD_PHYS-HDBSrse": checkForRSE(datasetsToCheck) # RSE = rucio storage element
    elif args.action == "check_RSE_for_TAPE_and_CERN-PROD_PHYS-HDBS":
        check_RSE_for_TAPE_and_CERN_PROD_PHYS_HDBS(datasetsToCheck)
    elif args.action == "make_rucioRules_spreadsheet": make_rucioRules_spreadsheet(datasetsToCheck)


    print( "All done!")


# user.chweber:user.chweber.ZdZd.production_20190416_79960920.ZdZd13TeV.signal_aa_mc16a_NTUP4L 
#              user.chweber.ZdZd.production_20190416_79960920.ZdZd13TeV.signal_aa_mc16a_NTUP4L